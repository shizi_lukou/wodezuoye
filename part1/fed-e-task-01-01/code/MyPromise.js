/*
尽可能还原 Promise 中的每一个 API, 并通过注释的方式描述思路和原理.
*/

// promise 有三种状态 等待、成功、失败
const PENDING = 'pending'
const FULFILLED = 'fullfilled'
const REJECTED = 'rejected'

// promise 的本质是一个类
class MyPromise {
    status = PENDING
    value = undefined
    reason = undefined
    // 成功和失败的回调(可以多次调用)
    successCallback = []
    failCallback = []
    // promise 需要传入一个执行器，执行器会立即执行
    constructor(executor) {
        try {
            executor(this.resolve,this.reject)
        } catch (e) {
            this.reject(e)
        }
    }
    resolve = value => {
        // 如果状态不是等待，则阻止向下执行
        if(this.status !== PENDING) return
        // 将状态设置为成功
        this.status = FULFILLED
        // 保存成功后的值
        this.value = value
        // 判断成功回调是否存在
        // this.successCallback && this.successCallback()
        while (this.successCallback.length){
            this.successCallback.shift()()
        }
    }
    reject = reason => {
        // 如果状态不是等待，则阻止向下执行
        if(this.status !== PENDING) return
        // 将状态设置为失败
        this.status = REJECTED
        // 保存失败后的原因
        this.reason = reason
        // 判断失败回调是否存在
        this.failCallback && this.failCallback()
        while (this.failCallback.length){
            this.failCallback.shift()()
        }
    }
    then (successCallback,failCallback) {
        // 判断then是否有参数
        successCallback = successCallback ? successCallback : value => value
        failCallback = failCallback ? failCallback : reason => throw reason
        // 链式调用需要返回promise
        let promise2 = new MyPromise( (resolve,reject) => {
            // 判断状态，执行对应的函数
            if(this.status === FULFILLED ){
                setTimeout(() => {
                    // 捕获错误
                    try {
                        let x = successCallback(this.value)
                        // 判断x的值是普通值还是promise对象
                        resolvePromise(x,resolve,reject,promise2)
                    }catch (e) {
                        reject(e)
                    }
                },0)
            }else if(this.status === REJECTED){
                setTimeout(() => {
                    // 捕获错误
                    try {
                        let x = failCallback(this.reason)
                        // 判断x的值是普通值还是promise对象
                        resolvePromise(x,resolve,reject,promise2)
                    }catch (e) {
                        reject(e)
                    }
                },0)
            }else {
                // 等待状态
                // 将回调存储起来

                this.successCallback.push(() => {
                    setTimeout(() => {
                        // 捕获错误
                        try {
                            let x = successCallback(this.value)
                            // 判断x的值是普通值还是promise对象
                            resolvePromise(x,resolve,reject,promise2)
                        }catch (e) {
                            reject(e)
                        }
                    },0)
                })
                this.failCallback.push(() => {
                    setTimeout(() => {
                        // 捕获错误
                        try {
                            let x = failCallback(this.reason)
                            // 判断x的值是普通值还是promise对象
                            resolvePromise(x,resolve,reject,promise2)
                        }catch (e) {
                            reject(e)
                        }
                    },0)
                })
            }
        })
        return promise2
    }
    // finally 无论成功或失败都会执行
    finally(callback){
        return this.then(value => {
            return MyPromise.resolve(callback()).then(() => value)
        }, reason => {
            return MyPromise.resolve(callback().then(() => { throw reason}))
        })
    }
    // promise.all
    static all(array){
        let result = []
        let index = 0
        return new MyPromise((resolve,reject) => {
            function addData(key,value) {
                result[key] = value
                index++
                if(index === result.length){
                    resolve(result)
                }
            }

            for(let i = 0; i < array.length; i++){
                if(array[i] instanceof MyPromise){
                    array[i].then(value => {addData(i,value)},reason => {reject(reason)})
                }else {
                    addData(i,array[i])
                }
            }
        })
    }
    // promise.resolve 如果是promise，直接返回。如果是普通值，返回包裹该值的promise对象
    static resolve(value){
        if(value instanceof MyPromise) return value
        return new MyPromise(resolve => resolve(value))
    }
    // 处理当前promise为失败的情况
    catch (failCallback) {
        return this.then(undefined,failCallback)
    }
}
// 判断x是否为promise对象
function resolvePromise(x,resolve,reject,promise2) {
    if(x instanceof MyPromise){
        // 判断x是否为当前promise 自己
        if(promise2 === x){
            return reject(new TypeError('Chaining cycle detected for promise # <promise>'))
        }
        x.then(resolve,reject)
    }else {
        resolve(x)
    }
}
